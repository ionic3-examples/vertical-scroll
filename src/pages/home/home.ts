import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  activity = 'data0'
  pxDate = '0px';
  valuePx = 0;
  DataId = 0;
  moment = moment;
  index = 0;
  data = '06-20-2020';
  horarios_array;
  
  items = [
    {dia:'27/06/2020', hora_inicial: "08:00"}, {dia:'28/06/2020', hora_inicial: "09:00"}, {dia:'29/06/2020', hora_inicial: "10:00"}
  ]

  public nomes;

  items2 = {
    "bruno": [
      {
        hora_inicial: '07:00',
        hora_final: "08:00"
      },
      {
        hora_inicial: '09:00',
        hora_final: "10:00"
      }
    ],
    "roberto": [
      {
        hora_inicial: '07:00',
        hora_final: "08:00"
      },
      {
        hora_inicial: '07:00',
        hora_final: "08:00"
      }
    ]
  }

  constructor(public navCtrl: NavController) {
    console.warn('items2', this.items2['bruno'])
    this.nomes = Object.keys(this.items2);
  }

  ionViewDidLoad() {

  }

  itemForward() {
    if((this.DataId + 1) != this.items.length) {
      this.DataId = this.DataId + 1;
      this.valuePx = this.valuePx + 60;
      this.pxDate = '-' +this.valuePx + 'px';
      this.activity = 'data' + this.DataId;
    }
  }

  itemBack() {
    if(this.DataId != 0) {
      this.DataId = this.DataId - 1;
      this.valuePx = this.valuePx - 60;
      this.pxDate = '-' +this.valuePx + 'px';
      this.activity = 'data' + this.DataId;
    }
  }

  horarios(nome) {
    console.log(this.items2[nome])
    this.horarios_array = this.items2[nome];
  }

}
